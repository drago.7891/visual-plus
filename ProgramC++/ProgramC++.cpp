﻿
#include <iostream>

using namespace std;

class Animal
{
	public:
		Animal(){}
		virtual void Sound() const = 0;
	
		
		
		virtual ~Animal() { cout << "Deleted Animal\n"; } 
};

class Cat : public Animal
{
	void Sound() const override
	{
		cout << "Meow\n";
	}
	~Cat() { cout << "Deleted Cat\n"; }
};

class Dog : public Animal
{
	void Sound() const override
	{
		cout << "Woof\n";
	}
	~Dog() { cout << "Deleted Dog\n"; }
};

class Monkey : public Animal
{
	void Sound() const override
	{
		cout << "U U U A A A\n";
	}
	~Monkey() { cout << "Deleted Monkey\n"; }
};

class Snake : public Animal
{
	void Sound() const override
	{
		cout << "SSSHHHHH\n";
	}
	~Snake() { cout << "Deleted Snake\n"; }
};

int main()
{
	const int i = 4;

	Animal* animals[i];
	animals[0] = new Cat;
	animals[1] = new Dog;
	animals[2] = new Monkey;
	animals[3] = new Snake;

	for (Animal* a : animals)
		a->Sound();
	for (int a = 0; a < i; a++)
	{
		delete animals[a];
	}
}